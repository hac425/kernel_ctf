PWD=$(pwd)
gcc -static -w test.c -o vuln_test
gcc -static -w exploit.c -o exploit
cp init.sh exploit vuln_test /home/haclh/kernel/x86_64_smep_bypass/fs/rootfs
cd /home/haclh/kernel/x86_64_smep_bypass/fs/rootfs
find . | cpio -o --format=newc > /home/haclh/kernel/x86_64_smep_bypass/fs/rootfs.img
cd $PWD
qemu-system-x86_64 -kernel /home/haclh/kernel/x86_64_smep_bypass/bzImage_KASLROFF -initrd /home/haclh/kernel/x86_64_smep_bypass/fs/rootfs.img -append "console=ttyS0  oops=panic panic=1" -enable-kvm -monitor /dev/null -m 64M --nographic  -smp cores=1,threads=1  -cpu kvm64,+smep --nographic -gdb tcp::1234
