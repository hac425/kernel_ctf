base_dir=$(pwd)
echo $base_dir
make clean
make
rm ~/busybox-1.27.1/_install/{test*,*.ko}
cp ./mknod.sh ./test* *.ko ~/busybox-1.27.1/_install
cd ~/busybox-1.27.1/_install
rm $base_dir/rootfs.img
find . | cpio -o --format=newc > $base_dir/rootfs.img
cd $base_dir
# echo "Enter to bootx"
# readPWD
qemu-system-x86_64 -kernel $base_dir/bzImage -initrd $base_dir/rootfs.img -append "console=ttyS0 root=/dev/ram oops=panic panic=1" -enable-kvm -monitor /dev/null -m 64M --nographic  -smp cores=1,threads=1  -cpu kvm64,+smep --nographic -gdb tcp::1234
